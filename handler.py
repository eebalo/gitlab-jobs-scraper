import json
import boto3
import json
import requests
from bs4 import BeautifulSoup
import datetime

def scrape(event, context):
  data = job_scrape()
  # file_name = f"productjobs-{data['date']}"
  print(json.dumps(data))
  # save_file_to_s3('ebay-daily-deals', file_name, data)

def job_scrape():
  page = requests.get("https://about.gitlab.com/jobs/apply/")
  content = BeautifulSoup(page.content, 'html.parser')
  department_jobs = content.find_all(attrs={"data-teams": "[4011048002, 4043190002]"})
  product_jobs = []
  for job in department_jobs:
    job_title = job.find("a").get_text()
    job_link = "https://about.gitlab.com/" + job.find("a").get('href')
    product_jobs.append({
      "title": job_title,
      "link": job_link
    })
  return product_jobs

def save_file_to_s3(bucket, file_name, data):
  s3 = boto3.resource('s3')
  obj = s3.Object(bucket, file_name)
  obj.put(Body=json.dumps(data))

def hello(event, context):
  body = {
    "message": "Go Serverless v1.0! Your function executed successfully!",
    "input": event
  }

  response = {
    "statusCode": 200,
    "body": json.dumps(body)
  }

  return response

  # Use this code if you don't use the http event with the LAMBDA-PROXY
  # integration
  """
  return {
    "message": "Go Serverless v1.0! Your function executed successfully!",
    "event": event
  }
  """
